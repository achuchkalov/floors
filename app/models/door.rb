class Door < ActiveRecord::Base
  belongs_to :room
  belongs_to :with, :class_name => "Room", :foreign_key => "with_id"

  validates :room, :position, :left, :top, presence: true
  validates :position, inclusion: {in: %w(top left right bottom)}

end
