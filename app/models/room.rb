class Room < ActiveRecord::Base
  belongs_to :floor
  has_many :doors

  # types: 'взрывоопасный', 'пожароопасный', 'малоопасный', 'негорючий'
  CONTAINER_TYPES = %w(safe little fire explosive)
  validates :containers_type, inclusion: {in: CONTAINER_TYPES}

  def right
    left+width
  end

  def bottom
    top+length
  end

  def neighbours
    if doors.present?
      doors.includes(:with).map(&:with).compact
    else
      Room.none
    end
  end

  def to_s
    "Room-#{id}"
  end

  def has_left_door?
    doors.where(position: 'left').present?
  end

  def has_right_door?
    doors.where(position: 'right').present?
  end

  def has_top_door?
    doors.where(position: 'top').present?
  end

  def has_bottom_door?
    doors.where(position: 'bottom').present?
  end

end
