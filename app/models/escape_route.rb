class EscapeRoute

  attr_reader :min_route, :link_matrix

  def initialize(floor)
    @floor = floor
    @rooms = floor.rooms
  end

  # expect room ids as params
  def build_route(from, to)
    build_link_matrix!
    find_min_route(from, to)
  end

  # private

  def find_min_route(from, to)
    @min_route = []
    @min_cost = 100000
    dfs(from, to, [from])
    @min_route
  end

  def dfs(from, to, route)
    if from.id == to.id
      if cost(route) < @min_cost
        @min_cost = cost(route)
        @min_route = route.dup
      end
      return
    end

    @link_matrix[from.id].each do |room|
      next if route.include?(room)
      route << room
      dfs(room, to, route)
      route.delete(room)
    end
  end

  def build_link_matrix!
    @link_matrix = {}

    @rooms.each do |room|
      @link_matrix[room.id] = []
      @rooms.each do |neighbour_candidate|
        if rooms_linked?(room, neighbour_candidate)
          @link_matrix[room.id] << neighbour_candidate
        end
      end
    end
  end

  def rooms_linked?(a, b)
    a.neighbours.include?(b) || b.neighbours.include?(a)
  end

  def cost(route)
    route.inject(0) {|sum, room| sum + room.danger_level*2}
  end

  def readable_link_matrix
    matrix = {}
    @link_matrix.each do |key, values|
      matrix[key] = values.map(&:id)
    end
    matrix
  end

end