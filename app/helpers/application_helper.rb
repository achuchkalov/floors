module ApplicationHelper
  def menu_link(name, url=nil, options={})
    options = options.dup
    active_item = options.delete(:active_item) || @active_menu_item
    i18n_scope = options.delete(:menu_code) || 'main_menu'
    text = options.delete(:text) || t(name, scope: i18n_scope)

    css_class = 'menu-link'
    css_class << ' active' if active_item == name
    content_tag :li, class: css_class do
      link_to text, url, options
    end
  end

  def mult
    30
  end

  def container_glyphicon_class(room)
    case room.containers_type
    when 'safe'
      'glyphicon-hdd'
    when 'little'
      'glyphicon-tree-conifer'
    when 'fire'
      'glyphicon-warning-sign'
    when 'explosive'
      'glyphicon-flash'
    end
  end

  def room_bg_class(room)
    ['bg-normal', 'bg-info', 'bg-warning', 'bg-danger'][room.danger_level]
  end
end
