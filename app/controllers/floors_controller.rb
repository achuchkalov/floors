class FloorsController < ApplicationController
  before_action :set_floor, only: [:show, :edit, :update, :destroy]
  before_action :set_active_menu_item

  def index
    @floors = Floor.order(:created_at)
  end

  def show
  end

  def new
    @floor = Floor.new
  end

  def edit
  end

  def create
    @floor = Floor.new(floor_params)

    if @floor.save
      redirect_to @floor, notice: 'Warehouse was successfully created.'
    else
      render :new
    end
  end

  def update
    if @floor.update(floor_params)
      redirect_to @floor, notice: 'Warehouse was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @floor.destroy
    redirect_to floors_url, notice: 'Warehouse was successfully destroyed.'
  end

  private
    def set_floor
      @floor = Floor.find(params[:id])
    end

    def floor_params
      params.require(:floor).permit(:name, :width, :length)
    end

    def set_active_menu_item
      @active_menu_item = :floors
    end
end
