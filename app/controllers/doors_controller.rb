class DoorsController < ApplicationController
  before_action :set_room
  before_action :set_door, only: [:show, :edit, :update, :destroy]

  # GET /doors
  def index
    @doors = @room.doors
  end

  # GET /doors/1
  def show
  end

  # GET /doors/new
  def new
    @door = @room.doors.build(top: 20, left: 20)
  end

  # GET /doors/1/edit
  def edit
  end

  # POST /doors
  def create
    @door = Door.new(door_params)

    if @door.save
      redirect_to [@room, :doors], notice: 'Door was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /doors/1
  def update
    if @door.update(door_params)
      redirect_to [@room, :doors], notice: 'Door was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /doors/1
  def destroy
    @door.destroy
    redirect_to [@room, :doors], notice: 'Door was successfully destroyed.'
  end

  private
    def set_room
      @room = Room.find(params[:room_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_door
      @door = Door.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def door_params
      params.require(:door).permit(:position, :room_id, :with_id, :top, :left)
    end
end
