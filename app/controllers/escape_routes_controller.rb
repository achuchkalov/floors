class EscapeRoutesController < ApplicationController
  def index
    from = Room.find(params[:from].to_i)
    to = Room.find(params[:to].to_i)
    route_builder = EscapeRoute.new(Floor.first)
    @route = route_builder.build_route(from, to)
    render layout: false
  end
end