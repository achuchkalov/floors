class RoomsController < ApplicationController
  before_action :set_room, only: [:show, :edit, :update, :destroy]
  before_action :set_active_menu_item

  def index
    @rooms = Room.order('created_at')
  end

  def show
  end

  def new
    @room = Room.new
  end

  def edit
  end

  def create
    @room = Room.new(room_params)

    if @room.save
      redirect_to rooms_path, notice: 'Room was successfully created.'
    else
      render :new
    end
  end

  def update
    if @room.update(room_params)
      update_danger_level(@room)
      redirect_to @room.floor, notice: 'Room was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @room.destroy
    redirect_to rooms_url, notice: 'Room was successfully destroyed.'
  end

  private
    def set_room
      @room = Room.find(params[:id])
    end

    def room_params
      params.require(:room).permit(
        :floor_id,
        :width,
        :length,
        :top,
        :left,
        :containers_count,
        :containers_type,
        :has_left_door,
        :has_right_door,
        :has_top_door,
        :has_bottom_door,
        :danger_level
      )
    end

    def set_active_menu_item
      @active_menu_item = :rooms
    end

    def update_danger_level(room)
      input_file = '/Users/sky/Documents/MATLAB/input1.txt'
      output_file = '/Users/sky/Documents/MATLAB/output1.txt'

      File.open(input_file, 'w') do |file|
        danger_level = Room::CONTAINER_TYPES.reverse.index(room.containers_type) + 1

        file.puts(danger_level) # тип склада
        file.puts(room.width * room.length * 50) # площадь
        file.puts(room.floor.rooms.size) # количество комнат
        file.puts(room.containers_count)
      end
      # `matlab -nodisplay -nojvm -r "calc_danger_level('#{input_file}', '#{output_file}'); exit"`
      `cd /Users/sky/Documents/MATLAB && matlab -nodisplay -nojvm -r "fire(1); exit"`
      danger_level = File.read(output_file).to_i
      room.update(danger_level: danger_level)
    end
end
