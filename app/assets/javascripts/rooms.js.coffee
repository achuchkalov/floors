update_containers_type = (form, room_container) ->
  type = form.find('#room_containers_type').val()
  containers = room_container.find('.container-icon')
  switch type
    when 'safe'
      containers.addClass('glyphicon-hdd')
    when 'little'
      containers.addClass('glyphicon-tree-conifer')
    when 'fire'
      containers.addClass('glyphicon-warning-sign')
    when 'explosive'
      containers.addClass('glyphicon-flash')

update_containers_count = (form, room_container) ->
  containers_count = parseInt(form.find('#room_containers_count').val())
  example = '<div class="container-icon glyphicon"></div>'
  containers_html = ''
  for i in [1..containers_count] by 1
    containers_html += example
  room_container.find('.containers').html(containers_html)

update_doors = (form, room_container) ->
  room_container.find('.door').addClass('hidden')
  checked = form.find(':checked')
  for side in ['left', 'right', 'top', 'bottom']
    if checked.filter("#room_has_#{side}_door").length
      room_container.find(".door-#{side}").removeClass('hidden')

$ ->
  $('.room-container .glyphicon-cog').click (e) ->
    e.preventDefault()
    $(this).closest('.room-container').find('.modal').modal('show')

  # $('.edit_room').submit ->
  #   form = $(this)
  #   room_container = form.closest('.room-container')

  #   form.closest('.modal').modal('hide')
  #   update_doors(form, room_container)
  #   update_containers_count(form, room_container)
  #   update_containers_type(form, room_container)
  #   true