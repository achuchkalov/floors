class AddContainersCountToRooms < ActiveRecord::Migration
  def change
    add_column :rooms, :containers_count, :integer, default: 5
  end
end
