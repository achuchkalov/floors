class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.references :floor, index: true
      t.integer :width
      t.integer :length
      t.integer :top
      t.integer :left

      t.timestamps
    end
  end
end
