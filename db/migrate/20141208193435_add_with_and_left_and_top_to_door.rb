class AddWithAndLeftAndTopToDoor < ActiveRecord::Migration
  def change
    add_reference :doors, :with, index: true
    add_column :doors, :left, :integer
    add_column :doors, :top, :integer
    remove_column :doors, :outer, :boolean
  end
end
