class AddDoorsToRoom < ActiveRecord::Migration
  def change
    add_column :rooms, :has_left_door, :boolean, default: false
    add_column :rooms, :has_right_door, :boolean, default: false
    add_column :rooms, :has_top_door, :boolean, default: false
    add_column :rooms, :has_bottom_door, :boolean, default: false
  end
end
