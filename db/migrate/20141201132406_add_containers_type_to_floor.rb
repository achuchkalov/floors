class AddContainersTypeToFloor < ActiveRecord::Migration
  def change
    add_column :floors, :containers_type, :string, default: 'safe'
  end
end
