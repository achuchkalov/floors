class MoveContainersTypeToRoom < ActiveRecord::Migration
  def change
    remove_column :floors, :containers_type, :string
    add_column :rooms, :containers_type, :string, default: 'safe'
  end
end
