class AddDangerLevelToRoom < ActiveRecord::Migration
  def change
    add_column :rooms, :danger_level, :integer, default: 0
  end
end
