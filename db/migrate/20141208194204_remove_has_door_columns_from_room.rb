class RemoveHasDoorColumnsFromRoom < ActiveRecord::Migration
  def change
    remove_column :rooms, :has_left_door, :boolean
    remove_column :rooms, :has_right_door, :boolean
    remove_column :rooms, :has_top_door, :boolean
    remove_column :rooms, :has_bottom_door, :boolean
  end
end
