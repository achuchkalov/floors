class CreateDoors < ActiveRecord::Migration
  def change
    create_table :doors do |t|
      t.boolean :outer
      t.string :position
      t.references :room, index: true

      t.timestamps
    end
  end
end
