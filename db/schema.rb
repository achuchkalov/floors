# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141208194204) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "doors", force: true do |t|
    t.string   "position"
    t.integer  "room_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "with_id"
    t.integer  "left"
    t.integer  "top"
  end

  add_index "doors", ["room_id"], name: "index_doors_on_room_id", using: :btree
  add_index "doors", ["with_id"], name: "index_doors_on_with_id", using: :btree

  create_table "floors", force: true do |t|
    t.string   "name"
    t.integer  "width"
    t.integer  "length"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rooms", force: true do |t|
    t.integer  "floor_id"
    t.integer  "width"
    t.integer  "length"
    t.integer  "top"
    t.integer  "left"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "containers_count", default: 5
    t.string   "containers_type",  default: "safe"
    t.integer  "danger_level",     default: 0
  end

  add_index "rooms", ["floor_id"], name: "index_rooms_on_floor_id", using: :btree

end
